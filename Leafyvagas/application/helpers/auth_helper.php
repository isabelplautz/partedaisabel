<?php
function permission(){ //permissão de páginas
    $ci = get_instance(); //mágica do codeigniter
    $loggeduser = $ci->session->set_userdata("logged_user"); //pegando um user loggado ai
    
    if(! $loggeduser) { //se não tiver loggado
        redirect("login"); //redirecionar para login
    }
    return $loggeduser; 
}  
?>