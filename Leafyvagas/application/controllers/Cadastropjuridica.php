<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastropjuridica extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $data['title'] = "Leafy Vagas - Cadastro de pessoa física";
    if($this->session->userdata("logged_user")){
      $this->load->view('templates/headerloggado', $data);
    } else{
      $this->load->view('templates/header', $data);
    }
  }

    public function index() {
		$data['title'] = "Leafy Vagas - Cadastro de pessoa jurídica";
      $this->load->view('pages/cadastropjuridica', $data);
    }
    public function cadastroefetuado(){
      $input = array("1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg", "9.jpg", "10.jpg", "11.jpg", "12.jpg");
      $rand_keys = array_rand($input, 2);

      $user = array(
        "tipouser" => 2,
        "nome" => $_POST["nome"],
        "cnpj" => $_POST["cnpj"],
        "email" => $_POST["email"],
        "numcel" => $_POST["numcel"],
        "senha" => md5($_POST["senha"]),
        "cep" => $_POST["cep"],
        "cidade" => $_POST["cidade"],
        "uf" => $_POST["uf"],
        "bairro" => $_POST["bairro"],
        "rua" => $_POST["rua"],
        "numlocal" => $_POST["numlocal"],
        "foto" => "/assets/img/imgperfil" . $input[$rand_keys[0]],
      );
      $this->load->model("cadastrouser_model");

      $this->cadastrouser_model->cadastroefetuado($user);
      redirect("/home/parabens");
    }

}