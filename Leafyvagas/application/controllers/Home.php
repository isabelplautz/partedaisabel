<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $data['title'] = "Leafy Vagas";
        if($this->session->userdata("logged_user")){
          $this->load->view('templates/headerloggado', $data);
        } else{
          $this->load->view('templates/header', $data);
        }
    }

    public function index() {
      $userloggado = $this->session->userdata("logged_user");
      $data['title'] = "Leafy Vagas";

      $this->load->view('pages/home', $data);
      $this->load->view('templates/footer', $data);
    }
    public function parabens() {
      $data['title'] = "Leafy Vagas - Parabéns";

      $this->load->view('pages/parabens', $data);
      $this->load->view('templates/footer', $data);
    }
}