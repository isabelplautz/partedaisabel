<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $data['title'] = "Leafy Vagas - Entrar";
    if($this->session->userdata("logged_user")){
      $this->load->view('templates/headerloggado', $data);
    } else{
      redirect('login');
    }
  }
//página perfil do user
    public function index() {
      $data['title'] = "Leafy Vagas - Perfil";
      $this->load->model("anuncio_model");
		  $data['ofertas'] = $this->anuncio_model->indexoferta();
      $this->load->model("anuncio_model");
		  $data['vagas'] = $this->anuncio_model->indexvaga();
      $this->load->model("cadastrouser_model");
		  $data['users'] = $this->cadastrouser_model->index();

        $this->load->view('pages/perfil', $data);
        $this->load->view('templates/footer', $data);
    }
// anuncios da pagina do perfil   
    public function meusanuncios(){
      $this->db->where("cod_user", $_SESSION["logged_user"]["cod_user"]);
			return $this->db->get("vagas")->result_array();
      return $this->db->get("ofertas")->result_array();
	  }
//editar perfil
    public function edit($cod_user){
      $this->load->model("perfil_model");
		  $data['users'] = $this->perfil_model->show($cod_user);
      $data['title'] = "Leafy Vagas - Editar perfil";

      if($data["users"]["cod_user"] == $_SESSION["logged_user"]["cod_user"]){ //se a pessoa da sessão for igual a que está no banco
        if($data["users"]["tipouser"] == 1){ // se ela for física
          $this->load->view('pages/editaperfilfisica', $data); //carrega o form física
        }else{
          $this->load->view('pages/editaperfiljur', $data);
        }
      }else{
        redirect("perfil");
      }
      
      $this->load->view('templates/footer', $data);
    }
//update das info do perfil
    public function update($cod_user){
      $this->load->model("perfil_model");
      if($_SESSION["logged_user"]["tipouser"] == 1){
        $user = array(
          "cod_user" => $_SESSION["logged_user"]["cod_user"],
          "tipouser" => 1,
          "nome" => $_POST["nome"],
          "cpf" =>  $_SESSION["logged_user"]["cpf"],
          "email" => $_POST["email"],
          "datanasc" => $_POST["datanasc"],
          "numcel" => $_POST["numcel"],
          "escolaridade" => $_POST["escolaridade"],
          "cep" => $_POST["cep"],
          "cidade" => $_POST["cidade"],
          "uf" => $_POST["uf"],
          "bairro" => $_POST["bairro"],
          "rua" => $_POST["rua"],
          "numlocal" => $_POST["numlocal"],
          "foto" => $_SESSION["logged_user"]["foto"],
        );
      }else{
        $user = array(
          "cod_user" => $_SESSION["logged_user"]["cod_user"],
          "tipouser" => 2,
          "nome" => $_POST["nome"],
          "cnpj" =>  $_SESSION["logged_user"]["cnpj"],
          "email" => $_POST["email"],
          "numcel" => $_POST["numcel"],
          "escolaridade" => "",
          "cep" => $_POST["cep"],
          "cidade" => $_POST["cidade"],
          "uf" => $_POST["uf"],
          "bairro" => $_POST["bairro"],
          "rua" => $_POST["rua"],
          "numlocal" => $_POST["numlocal"],
          "foto" => $_SESSION["logged_user"]["foto"],
        );
      }
      $this->perfil_model->update($cod_user, $user);
      redirect("perfil");
    }

//delete usuario
public function delete($cod_user){
  $this->load->model("perfil_model");
  $this->perfil_model->destroy($cod_user);
  $this->perfil_model->destroyvaga($cod_user);
  $this->perfil_model->destroyoferta($cod_user);
  $this->session->unset_userdata("logged_user");
  redirect("home");
}

//editar vaga
    public function editvaga($cod_v){
      $this->load->model("perfil_model");
		  $data['vagas'] = $this->perfil_model->showvaga($cod_v);
      $data['title'] = "Leafy Vagas - Editar vaga";

      if($data["vagas"]["cod_user"] == $_SESSION["logged_user"]["cod_user"]){
          $this->load->view('pages/editavaga', $data); 
      }else{
        redirect("perfil");
      }
      $this->load->view('templates/footer', $data);
    }
//editar oferta
    public function editoferta($cod_o){

      $this->load->model("perfil_model");
		  $data['ofertas'] = $this->perfil_model->showoferta($cod_o);
      $data['title'] = "Leafy Vagas - Editar oferta";

      if($data["ofertas"]["cod_user"] == $_SESSION["logged_user"]["cod_user"]){
          $this->load->view('pages/editaoferta', $data); 
      }else{
        redirect("perfil");
      }
      $this->load->view('templates/footer', $data);
    }
// update vaga
    public function updatevaga($cod_v){
      $this->load->model("perfil_model");
      $vaga = $this->input->post();
      $locais = '';
      $cont = 0;
      foreach($vaga['locais'] as $local){
        if($cont != 0){
          $locais .= ', ' . $local;
        }else{
          $locais .= $local;
        }
        $cont ++; 
      }
      $vaga['locais'] = $locais;
      $this->perfil_model->updatevaga($cod_v, $vaga);
      redirect("perfil");
    }
// update oferta   
    public function updateoferta($cod_o){
      $this->load->model("perfil_model");
      $oferta = $this->input->post();
      $locais = '';
      $cont = 0;
      foreach($oferta['locais'] as $local){
        if($cont != 0){
          $locais .= ', ' . $local;
        }else{
          $locais .= $local;
        }
        $cont ++; 
      }
      $oferta['locais'] = $locais;
      $this->perfil_model->updateoferta($cod_o, $oferta);
      redirect("perfil");
    }

//delete vaga
    public function deletevaga($cod_v){
      $this->load->model("perfil_model");
      $this->perfil_model->destroyvagaind($cod_v);
      redirect("perfil");
    }
//delete oferta
    public function deleteoferta($cod_o){
      $this->load->model("perfil_model");
      $this->perfil_model->destroyofertaind($cod_o);
      redirect("perfil");
    }
}