<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vagas extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $data['title'] = "Leafy Vagas - Vagas";
    if($this->session->userdata("logged_user")){
      $this->load->view('templates/headerloggado', $data);
    } else{
      $this->load->view('templates/header', $data);
    }
  }

    public function index() {
      $data['title'] = "Leafy Vagas - Vagas";
      
      $this->load->model("anuncio_model");
		  $data['vagas'] = $this->anuncio_model->indexvaga();
      $data['users'] = $this->db->get("users")->result_array();
      
      $this->load->view('pages/vagas', $data);
      $this->load->view('templates/footer', $data);
    
    }

    public function pesquisarvagas() {
      $this->load->model("pesquisar_model");
      
      $data['title'] = "Resultado da pesquisa por: ".$_POST["pesquisa"];
      $data['result'] = $this->pesquisar_model->pesquisarvagas($_POST);
      $data['users'] = $this->db->get("users")->result_array();
      
      $this->load->view('pages/resultvaga', $data);
      $this->load->view('templates/footer', $data);
    
    }
}