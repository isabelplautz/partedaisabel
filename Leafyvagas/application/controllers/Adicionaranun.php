<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adicionaranun extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $data['title'] = "Leafy Vagas - Adicionar anúncio"; 
    if($this->session->userdata("logged_user")){ //se o usuário está loggado
      $this->load->view('templates/headerloggado', $data); //carrega o header loggado
    } else{
      redirect('login');
    }
  }

    public function index() {
      $data['title'] = "Leafy Vagas - Adicionar anúncio";
      //carregando as views
      $this->load->view('pages/adicionaranun', $data); 
      $this->load->view('templates/footer', $data);
    }

    public function cadastrovaga(){
      $vaga = $this->input->post();
      $locais = '';
      $cont = 0;
      foreach($vaga['locais'] as $local){
        if($cont != 0){
          $locais .= ', ' . $local;
        }else{
          $locais .= $local;
        }
        $cont ++; 
      }
      $vaga['locais'] = $locais;
      $vaga['cod_user'] = $this->session->userdata('logged_user')['cod_user'];

      $this->load->model("anuncio_model"); // carrega a model
      $this->anuncio_model->cadastrovaga($vaga); // essa model cadastra a vaga no banco
      redirect("vagas"); //volta pra página vagas
    } 
    public function cadastrooferta(){
      $oferta = $this->input->post();
      $locais = '';
      $cont = 0;
      foreach($oferta['locais'] as $local){
        if($cont != 0){
          $locais .= ', ' . $local;
        }else{
          $locais .= $local;
        }
        $cont ++; 
      }
      $oferta['locais'] = $locais;
      $oferta['cod_user'] = $this->session->userdata('logged_user')['cod_user'];
      
      $this->load->model("anuncio_model");
      $this->anuncio_model->cadastrooferta($oferta);
      redirect("ofertas");
    } 
}