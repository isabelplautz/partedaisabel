<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  public function __construct() {
    parent::__construct();

    $data['title'] = "Leafy Vagas - Entrar";
    if($this->session->userdata("logged_user")){
      $this->load->view('templates/headerloggado', $data);
    } else{
      $this->load->view('templates/header', $data);
    }
  }
    public function index() {
      $data['title'] = "Leafy Vagas - Entrar";
      
        $this->load->view('pages/login', $data);
        $this->load->view('templates/footer', $data);
    }

    public function store(){

      $this->load->model("login_model");
      $email = $_POST["email"];
      $senha = md5($_POST["senha"]);
      $user =  $this->login_model->store($email, $senha);
      if($user){
        $this->session->set_userdata("logged_user", $user);
        redirect("home");
      } else{
        redirect("login");
      }
    } 

    public function logout(){
      $this->session->unset_userdata("logged_user");
      redirect("login");
    }

}