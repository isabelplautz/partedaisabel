<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ofertas extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $data['title'] = "Leafy Vagas - Ofertas";
    if($this->session->userdata("logged_user")){
      $this->load->view('templates/headerloggado', $data);
    } else{
      $this->load->view('templates/header', $data);
    }
  }

    public function index() {
      $data['title'] = "Leafy Vagas - Ofertas";

      $this->load->model("anuncio_model");
      $data['ofertas'] = $this->anuncio_model->indexoferta();
      $data['users'] = $this->db->get("users")->result_array();
      
      $this->load->view('pages/ofertas', $data);
      $this->load->view('templates/footer', $data);
    }

    public function pesquisarofertas() {
      $this->load->model("pesquisar_model");
      $data['title'] = "Resultado da pesquisa por: ".$_POST["pesquisa"];
      $data['result'] = $this->pesquisar_model->pesquisarofertas($_POST);
      $data['users'] = $this->db->get("users")->result_array();

      $this->load->view('pages/resultoferta', $data);
      $this->load->view('templates/footer', $data);
    
    }
}