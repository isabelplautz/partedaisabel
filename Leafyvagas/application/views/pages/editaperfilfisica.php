<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<body>
    <div class="mainbox">
        <div class="cadfisicbox">
        <?php $userloggado = $this->session->userdata("logged_user"); ?>
            <h1>Editar - Pessoa Física</h1>
            <br>
            <hr class="solid">
            <br>
                <form id="cadpfisica" action="<?= base_url('perfil/update')."/".$userloggado['cod_user'] ?>" method="post">
                    <input type="text" id="nome" name="nome" placeholder="Nome Completo" value="<?= isset($userloggado) ? $userloggado['nome'] : "" ?>" required><br><br>
                    <input class="desativado" type="text" id="cpf" name="cpf" placeholder="CPF*" maxlength="14" disabled value="<?= isset($userloggado) ? $userloggado['cpf'] : "" ?>"><br><br>
                    <input type="email" id="email" name="email" placeholder="Email*" value="<?= isset($userloggado) ? $userloggado['email'] : "" ?>" required><br><br>
                    <div class="formmetade">
                        <div><input type="date" id="datanasc" name="datanasc" placeholder="Data de Nascimento*"  value="<?= isset($userloggado) ? $userloggado['datanasc'] : "" ?>" required></div>
                        <div><input type="tel" id="numcel" name="numcel" placeholder="Número de celular*" maxlength="14" onkeydown="javascript: fMasc( this, mTel );" value="<?= isset($userloggado) ? $userloggado['numcel'] : "" ?>" required></div>
                    </div>
                    <br>
                    <select id="escolaridade" name="escolaridade" form="cadpfisica">
                        <option selected ><?= isset($userloggado) ? $userloggado['escolaridade'] : "" ?></option>
                        <option value="Fundamental - Incompleto">Fundamental - Incompleto</option>
                        <option value="Fundamental - Completo">Fundamental - Completo</option>
                        <option value="Médio - Incompleto">Médio - Incompleto</option>
                        <option value="Médio - Completo">Médio - Completo</option>
                        <option value="Superior - Incompleto">Superior - Incompleto</option>
                        <option value="Superior - Completo">Superior - Completo</option>
                        <option value="Pós-graduação - Incompleto">Pós-graduação - Incompleto</option>
                        <option value="Pós-graduação - Completo">Pós-graduação - Completo</option>
                        <option value="Pós-graduação mestrado - Incompleto">Pós-graduação mestrado - Incompleto</option>
                        <option value="Pós-graduação mestrado - Completo">Pós-graduação mestrado - Completo</option>
                        <option value="Pós-graduação doutorado - Incompleto">Pós-graduação doutorado - Incompleto</option>
                        <option value="Pós-graduação doutorado - Completo">Pós-graduação doutorado - Completo</option>
                        </select>
                        <br><br>
                    <p style="color: #727272;">Endereço</p>
                    <input name="cep" type="text" id="cep" size="10" maxlength="9" placeholder="CEP*" onblur="pesquisacep(this.value);" value="<?= isset($userloggado) ? $userloggado['cep'] : "" ?>" onkeydown="javascript: fMasc( this, mCEP );" required><br><br>
                    <div class="formuni">
                        <div style="width: 38%;"><input name="cidade" type="text" id="cidade" size="40" placeholder="Cidade" value="<?= isset($userloggado) ? $userloggado['cidade'] : "" ?>" required><br><br></div>
                        <div style="width: 18%;"><input name="uf" type="text" id="uf" size="2" placeholder="UF" value="<?= isset($userloggado) ? $userloggado['uf'] : "" ?>" required><br><br></div>
                        <div style="width: 38%;"><input name="bairro" type="text" id="bairro" size="40" placeholder="Bairro" value="<?= isset($userloggado) ? $userloggado['bairro'] : "" ?>" required><br><br></div>
                    </div>
                    <div class="formuni">
                        <div style="width: 78%;"><input name="rua" type="text" id="rua" size="60" placeholder="Rua" value="<?= isset($userloggado) ? $userloggado['rua'] : "" ?>" required></div>
                        <div style="width: 18%;"><input name="numlocal" type="int" id="numlocal" size="2" placeholder="Número*" value="<?= isset($userloggado) ? $userloggado['numlocal'] : "" ?>" required></div>
                    </div><br><br>
                  <!--  <input type="password" id="senha" name="senha" placeholder="Senha atual*" required><br><br>
                    <input type="password" id="senha" name="senha" placeholder="Senha nova*"><br><br>
                    <input type="password" id="confsenha" placeholder="Confirmar Senha nova*"><br><br>
                    <p style="color:#B6B6B6;">* Itens obrigatórios</p><br> !-->
                    <input class="sendbutton" type="submit" id="enviar" value="SALVAR">
                </form>
        </div>
    </div>
    
</body>