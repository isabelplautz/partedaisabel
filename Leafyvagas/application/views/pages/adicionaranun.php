
<link href="<?=base_url('/assets/css/formanuncio.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<body>
  <div class="mytabs">
    <input type="radio" id="tabvagas" name="mytabs" checked="checked">
    <label class="tabtoptitulo" for="tabvagas">Vagas</label>
    <div class="tab">
      <form id="anunciovaga" action="<?= base_url() ?>adicionaranun/cadastrovaga" method="post">
        <input type="text" id="titulo" name="titulo" placeholder="Título da Vaga*" required>
        <br><br>
        <select id="cidade" name="cidade" form="anunciovaga" required>
          <option selected>Cidade</option>
          <option value="Araricá">Araricá</option>
          <option value="Campo Bom">Campo Bom</option>
          <option value="Dois Irmãos">Dois Irmãos</option>
          <option value="Nova Hartz">Nova Hartz</option>
          <option value="Novo Hamburgo">Novo Hamburgo</option>
          <option value="Sapiranga">Sapiranga</option>
        </select> 
        <br><br>
        <div class="checkboxlocais">
          <p class="titulolocais">Locais de trabalho:</p>
          <div><input type="checkbox" name="locais[]"  value="Presencial"><label class="titulolocais">Presencial</label></div>
          <div><input type="checkbox" name="locais[]" value="Híbrido"><label class="titulolocais">Híbrido</label></div>
          <div><input type="checkbox" name="locais[]" value="Home Office"><label class="titulolocais">Home Office</label></div>
        </div>
        <br><br>
        <input type="number" id="salario" name="salario" placeholder="Média salarial*" required>
        <br><br>
        <select id="tipovaga" name="tipovaga" form="anunciovaga" required>
          <option selected>Tipo de vaga</option>
          <option value="Meio período">Meio período</option>
          <option value="Estágio">Estágio</option>
          <option value="Integral">Integral</option>
          <option value="CLT">CLT</option>
          <option value="Freelance">Freelance</option>
        </select> 
        <br><br>
        <input type="text" id="jornada" name="jornada" placeholder="Jornada de Trabalho (EX: 40h semanais)">
        <br><br>
        <select id="escolaridade" name="escolaridade" form="anunciovaga">
            <option selected>Escolaridade mínima</option>
            <option value="Fundamental - Incompleto">Fundamental - Incompleto</option>
            <option value="Fundamental - Completo">Fundamental - Completo</option>
            <option value="Médio - Incompleto">Médio - Incompleto</option>
            <option value="Médio - Completo">Médio - Completo</option>
            <option value="Superior - Incompleto">Superior - Incompleto</option>
            <option value="Superior - Completo">Superior - Completo</option>
            <option value="Pós-graduação - Incompleto">Pós-graduação - Incompleto</option>
            <option value="Pós-graduação - Completo">Pós-graduação - Completo</option>
            <option value="Pós-graduação mestrado - Incompleto">Pós-graduação mestrado - Incompleto</option>
            <option value="Pós-graduação mestrado - Completo">Pós-graduação mestrado - Completo</option>
            <option value="Pós-graduação doutorado - Incompleto">Pós-graduação doutorado - Incompleto</option>
            <option value="Pós-graduação doutorado - Completo">Pós-graduação doutorado - Completo</option>
          </select>
          <br><br>
          <textarea rows="5" id="descvaga" name="descvaga" placeholder="Descrição da vaga*" required></textarea>
          <br><br>
          <input type="text" id="formacontato" name="formacontato" placeholder="Forma de contato*" required>
          <br><br>
          <p style="color:#B6B6B6;">* Itens obrigatórios</p>
          <br>
          <input class="sendbutton" type="submit" id="enviar" value="CADASTRAR">
          </form>
    </div>

   <input type="radio" id="tabofertas" name="mytabs">
    <label class="tabtoptitulo" for="tabofertas">Ofertas</label>
    <div class="tab">
    <form id="anunciooferta" action="<?= base_url() ?>adicionaranun/cadastrooferta" method="post" enctype="multipart/form-data">
      <input type="text" id="titulo" name="titulo" placeholder="Título da Oferta*" required>
      <br><br>
      <select id="cidade" name="cidade" form="anunciooferta" required>
          <option selected>Cidade</option>
          <option value="Araricá">Araricá</option>
          <option value="Campo Bom">Campo Bom</option>
          <option value="Dois Irmãos">Dois Irmãos</option>
          <option value="Nova Hartz">Nova Hartz</option>
          <option value="Novo Hamburgo">Novo Hamburgo</option>
          <option value="Sapiranga">Sapiranga</option>
        </select> 
        <br><br>
        <div class="checkboxlocais">
          <p class="titulolocais">Locais de trabalho:</p>
          <div><input type="checkbox" name="locais[]"  value="Presencial"><label class="titulolocais">Presencial</label></div>
          <div><input type="checkbox" name="locais[]" value="Híbrido"><label class="titulolocais">Híbrido</label></div>
          <div><input type="checkbox" name="locais[]" value="Home Office"><label class="titulolocais">Home Office</label></div>
        </div>
        <br><br>
      <br><br>
      <select id="tipooferta" name="tipooferta" form="anunciooferta" required>
        <option selected>Tipo de Oferta</option>
        <option value="Freelancer">Freelancer</option>
        <option value="Autônomo">Autônomo</option>
        <option value="Procurando Emprego">Procurando Emprego</option>
        <option value="Microempreendedor">Microempreendedor</option>
      </select>
      <br><br>
      <div class="formmetade">
        <div><input type="number" id="preco" name="preco" placeholder="Valor (R$)" required></div>
        <div><input type="text" id="tipopreco" name="tipopreco" placeholder="Tipo (ex: por hora, por serviço, por semana, etc.)" required></div>
      </div>
      <br><br>
      <textarea rows="5" id="descricao" name="descricao" placeholder="Descrição da oferta*" required></textarea>
      <br><br>
      <input type="text" id="formacontato" name="formacontato" placeholder="Forma de contato*" required>
      <br><br>
      <p style="color:#B6B6B6;">* Itens obrigatórios</p>
      <input class="sendbutton" type="submit" id="enviar" value="CADASTRAR">
    </form>
    </div>

  </div>
</body>