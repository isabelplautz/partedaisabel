<link href="<?=base_url('/assets/css/anuncios.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>

<body>
    <h2 style="margin: 3%;"><?php echo $title ?></h2>
    <div class="anuncios">
        <br>
        <div class="filtros">
            <br>
            <form action="<?=base_url('ofertas/pesquisarofertas')?>" method="post">
                <input placeholder="Pesquise por nome, tipo da vaga ou cidade..." id="pesquisa" name="pesquisa" class="pesquisar" type="text">
            </form>
            <br>
        </div>
        <div class="results">
            <?php if(count($result) == 0){?>
               <h2>Não encontrei nada :(</h2>
            <?php } else{ ?>
                <div class="anuncioslista">
                    <?php foreach($result as $oferta) : ?>
                        <div class="anunciodiv">
                                <div class="infovaga">
                                    <h2><?= $oferta['titulo'] ?></h2>
                                    <p>Código da oferta #<?= $oferta['cod_o'] ?></p><br>
                                    <strong>Anunciante: </strong><?php foreach($users as $user) :
                                    if($oferta["cod_user"] == $user["cod_user"]) {
                                        echo $user["nome"]."<br>";
                                        echo "<strong>Escolaridade: </strong>"; 
                                        echo $user["escolaridade"]."<br>";
                                        } ?>
                                <?php endforeach ; ?>
                                    <strong>Cidade: </strong><?php 
                                        if($oferta["cidade"] == "Cidade") {?>
                                            Não informado<br>
                                        <?php } else{ ?>
                                            <?= $oferta["cidade"]?><br>
                                        <?php } ?>
                                        <strong>Locais de Trabalho: </strong><?php 
                                        if($oferta["locais"] == "") {?>
                                            Não informado<br>
                                        <?php } else{ ?>
                                            <?= $oferta["locais"]?><br>
                                        <?php } ?>
                                        <strong>Tipo de Oferta: </strong><?php 
                                        if($oferta["tipooferta"] == "Tipo de vaga") {?>
                                            Não informado<br>
                                        <?php } else{ ?>
                                            <?= $oferta["tipooferta"]?><br>
                                        <?php } ?>
                                    <strong>Valor: </strong>R$<?= $oferta['preco']." ".$oferta['tipopreco'] ?><br>
                                    <?= $oferta['descricao'] ?><br><br>
                                    <strong>Forma de contato: </strong><?= $oferta['formacontato'] ?><br>
                                </div>
                        </div>
                    <?php endforeach ; ?>
                </div>
            <?php } ?>
        </div>
    </div>
</body>   