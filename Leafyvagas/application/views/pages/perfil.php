<link href="<?=base_url('/assets/css/perfil.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/anuncios.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<script>
	function goDelete(id) {
		
		if(confirm("Tem certeza que deseja excluir sua conta? :'(")) {
			window.location.href = window.location.href + '/delete/'+id;
		} else {
			alert("Show de bola! Pode ficar aí :)");
			return false;
		}
	}
    function goDeleteVaga(id) {
		
		if(confirm("Tem certeza que deseja excluir essa vaga? :'(")) {
			window.location.href = window.location.href + '/deletevaga/'+id;
		} else {
			alert("Ah blz, se liga hein");
			return false;
		}
	}
    function goDeleteOferta(id) {
		
		if(confirm("Tem certeza que deseja excluir essa oferta? :'(")) {
			window.location.href = window.location.href + '/deleteoferta/'+id;
		} else {
			alert("Ah blz, se liga hein");
			return false;
		}
	}
</script>
<body>
    <div class="tudoperfil">
        <?php $userloggado = $this->session->userdata("logged_user"); ?>
            <div class="perfil">
                <div class="perfilfotoinfo">
                    <div>
                        <img class="imgperfil" src="<?=base_url($userloggado['foto'])?>">
                    </div>
                    <div class="infototalperfil">
                        <h2><?= $userloggado['nome'] ?></h2>
                        <p class="infoperfil"><?= $userloggado['email'] ?></p>
                        <p class="infoperfil"><?= $userloggado['cidade'] ?></p>
                        <p class="infoperfil"><?= $userloggado['escolaridade'] ?></p>
                    </div>
                </div>
                <div class="perfilbottom">
                    <div class="buttonsperfil">
                        <div class="smallbutton"><a href="<?= base_url('perfil/edit/').$_SESSION["logged_user"]["cod_user"];?>"><i class="fas fa-user-edit"></i></a></div>
                        <div class="smallbutton"><a href="javascript:goDelete(<?= $_SESSION["logged_user"]["cod_user"]; ?>)"><i class="fas fa-trash-alt"></i></a></div>
                    </div> 
                </div>
            </div>
        <div class="anuncioslista">
            <div class="mytabs">
            <input type="radio" id="tabvagas" name="mytabs" checked="checked">
            <label for="tabvagas">Vagas</label>
            <div class="tab">
                <?php foreach($vagas as $vaga) : ?>
                    <?php if($_SESSION["logged_user"]["cod_user"] == $vaga["cod_user"]) : ?>
                        <div class="anunciodiv">
                            <div class="inneranunciodiv">
                                <div class="infoanun">
                                    <h2><?= $vaga['titulo'] ?></h2>
                                    <p>Código da vaga #<?= $vaga['cod_v'] ?></p><br>
                                    <p><?= $vaga['descvaga'] ?></p>
                                </div>
                                <div class="perfilbottom">
                                    <div class="buttonsperfil">
                                        <div class="smallbutton"><a href="<?= base_url('perfil/editvaga/').$vaga["cod_v"];?>"><i class="fas fa-edit"></i></a></div>
                                        <div class="smallbutton"><a href="javascript:goDeleteVaga(<?= $vaga["cod_v"]; ?>)"><i class="fas fa-trash-alt"></i></a></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach ; ?>
                
            </div>

            <input type="radio" id="tabofertas" name="mytabs">
            <label for="tabofertas">Ofertas</label>
            <div class="tab">
                <?php foreach($ofertas as $oferta) : ?>
                    <?php if($_SESSION["logged_user"]["cod_user"] == $oferta["cod_user"]) : ?>
                        <div class="anunciodiv">
                        <div class="inneranunciodiv">
                            <div class="infoanun">
                                <h2><?= $oferta['titulo'] ?></h2>
                                <p>Código da oferta #<?= $oferta['cod_o'] ?></p><br>
                                <p><?= $oferta['descricao'] ?></p>
                            </div>
                            <div class="anunciobottom">
                                <div class="buttonsperfil">
                                    <div class="smallbutton"><a href="<?= base_url('perfil/editoferta/').$oferta["cod_o"];?>"><i class="fas fa-edit"></i></a></div>
                                    <div class="smallbutton"><a href="javascript:goDeleteOferta(<?= $oferta["cod_o"]; ?>)"><i class="fas fa-trash-alt"></i></a></div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach ; ?>
            </div>

    </div>
        </div>
    </div>
</body>
