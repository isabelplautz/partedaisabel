<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<div class="logincad">
    <div class="mainbox">
        <h1>Fazer Login</h1>
        <br>
        <hr class="solid">
        <br>
        <div id="allloginform" class="loginformdiv">
            <div class="headerlogin"></div>
            <div class="loginform">
                <form action="<?= base_url() ?>login/store" method="post">
                    <input type="email" name="email" placeholder="E-mail"><br><br>
                    <input type="password" name="senha" id="senha" placeholder="Senha" />
                    <br><br>
                    
                    <input class="normalbuttonfull" type="submit" value="Entrar">
                </form>
            </div>
        </div>
    </div>
    <div class="mainbox">
        <h1>Fazer Cadastro</h1>
        <br>
        <hr class="solid">
        <br>
        <div style="display: grid;">
            <a href="<?= base_url('')?>cadastropfisica" class="normalbuttonfull">PESSOA FÍSICA</a>
            <br><br>
            <a href="<?= base_url('')?>cadastropjuridica" class="normalbuttonfull">PESSOA JURÍDICA</a>
        </div>
        
    </div>
</div>