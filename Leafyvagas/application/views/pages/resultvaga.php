<link href="<?=base_url('/assets/css/anuncios.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>

<body>
    <h2 style="margin: 3%;"><?php echo $title ?></h2>
    <div class="anuncios">
        <br>
        <div class="filtros">
            <br>
            <form action="<?=base_url('vagas/pesquisarvagas')?>" method="post">
                <input placeholder="Pesquise por nome, tipo da vaga ou cidade..." id="pesquisa" name="pesquisa" class="pesquisar" type="text">
            </form>
            <br>
        </div>
        <div class="results">
            <?php if(count($result) == 0){?>
               <h2>Não encontrei nada :(</h2>
            <?php } else{ ?>
					<div class="anuncioslista">
				<?php foreach($result as $vaga) : ?>
					<div class="anunciodiv">
                            <div class="infovaga">
                                <h2><?= $vaga['titulo'] ?></h2>
                                <p>Código da vaga #<?= $vaga['cod_v'] ?></p><br>
                                <strong>Contratante: </strong><?php foreach($users as $user) :
                                    if($vaga["cod_user"] == $user["cod_user"]) {?>
                                        <?= $user["nome"]?><br>
                                    <?php } ?>
                                <?php endforeach ; ?>
                                <strong>Cidade: </strong><?php 
                                    if($vaga["cidade"] == "Cidade") {?>
                                        Não informado<br>
                                    <?php } else{ ?>
                                        <?= $vaga["cidade"]?><br>
                                    <?php } ?>
                                    <strong>Locais de Trabalho: </strong><?php 
                                    if($vaga["locais"] == "") {?>
                                        Não informado<br>
                                    <?php } else{ ?>
                                        <?= $vaga["locais"]?><br>
                                    <?php } ?>
                                <strong>Média salarial: </strong>R$<?= $vaga['salario'] ?><br>
                                <strong>Tipo da vaga: </strong><?php 
                                    if($vaga["tipovaga"] == "Tipo de vaga") {?>
                                        Não informado<br>
                                    <?php } else{ ?>
                                        <?= $vaga["tipovaga"]?><br>
                                    <?php } ?>
                                    <strong>Jornada de Trabalho: </strong><?php 
                                    if($vaga["jornada"] == "") {?>
                                        Não informado<br>
                                    <?php } else{ ?>
                                        <?= $vaga["jornada"]?><br>
                                    <?php } ?>
                                <strong>Escolaridade mínima: </strong><?= $vaga['escolaridade'] ?><br><br>
                                <?= $vaga['descvaga'] ?><br><br>
                                <strong>Forma de contato: </strong><?= $vaga['formacontato'] ?><br>
                            </div>
                    </div>
				<?php endforeach ; ?>
            </div><?php } ?>
        </div>
    </div>
</body>   