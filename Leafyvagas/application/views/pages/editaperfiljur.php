<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<body>
    <div class="mainbox">
        <div class="cadfisicbox">
        <?php $userloggado = $this->session->userdata("logged_user"); ?>
            <h1>Editar - Pessoa Jurídica</h1>
            <hr class="solid">
            <form id="cadpjuridica" action="<?= base_url('perfil/update')."/".$userloggado['cod_user'] ?>" method="post">
                <input type="text" id="nome" name="nome" placeholder="Razão social*" value="<?= isset($userloggado) ? $userloggado['nome'] : "" ?>" required><br><br>
                <input class="desativado" type="text" id="cnpj" name="cnpj" placeholder="CNPJ*" onblur="validarCNPJ(this.value);" maxlength="18" onkeydown="javascript: fMasc( this, mCNPJ );" disabled  value="<?= isset($userloggado) ? $userloggado['cnpj'] : "" ?>" ><br><br>
                <input type="email" id="email"name="email"  placeholder="Email*" value="<?= isset($userloggado) ? $userloggado['email'] : "" ?>"><br><br>
                <input type="numcel" name="numcel" placeholder="Número de celular*" maxlength="14" onkeydown="javascript: fMasc( this, mTel );" value="<?= isset($userloggado) ? $userloggado['numcel'] : "" ?>"><br><br>
                <br>
               <!-- <input type="password" id="senha" name="senha" placeholder="Senha*" required><br><br>
                <input type="password" id="confsenha" placeholder="Confirmar Senha*" required><br><br>!-->
                <p>Endereço</p>
                <input name="cep" type="text" id="cep" size="10" maxlength="9" placeholder="CEP*" onblur="pesquisacep(this.value);" onkeydown="javascript: fMasc( this, mCEP );" value="<?= isset($userloggado) ? $userloggado['cep'] : "" ?>"><br><br>
                <div class="formuni">
                    <div style="width: 38%;"><input name="cidade" type="text" id="cidade" size="40" placeholder="Cidade" value="<?= isset($userloggado) ? $userloggado['cidade'] : "" ?>"><br><br></div>
                    <div style="width: 18%;"><input name="uf" type="text" id="uf" size="2" placeholder="UF" value="<?= isset($userloggado) ? $userloggado['uf'] : "" ?>"><br><br></div>
                    <div style="width: 38%;"><input name="bairro" type="text" id="bairro" size="40" placeholder="Bairro" value="<?= isset($userloggado) ? $userloggado['bairro'] : "" ?>"><br><br></div>
                </div>
                <div class="formuni">
                    <div style="width: 78%;"><input name="rua" type="text" id="rua" size="60" placeholder="Rua" value="<?= isset($userloggado) ? $userloggado['rua'] : "" ?>"></div>
                    <div style="width: 18%;"><input name="numlocal" type="int" id="numlocal" size="2" placeholder="Número*" value="<?= isset($userloggado) ? $userloggado['numlocal'] : "" ?>"></div>
                </div>
                <p style="color:#B6B6B6;">* Itens obrigatórios</p><br>
                <input class="sendbutton" type="submit" id="enviar" value="SALVAR">
                </form>
        </div>
    </div>

</body>