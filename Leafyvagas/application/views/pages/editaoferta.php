
<link href="<?=base_url('/assets/css/formanuncio.css')?>" rel="stylesheet" type="text/css"/>
<link href="<?=base_url('/assets/css/formstyle.css')?>" rel="stylesheet" type="text/css"/>
<body>
  <div class="mytabs">
    <input type="radio" id="tabvagas" name="mytabs" checked="checked">
    <label class="tabtoptitulo" for="tabvagas">Editar Oferta #<?=$ofertas['cod_o']?></label>
    <div class="tab">
    <form id="anunciooferta" action="<?= base_url('perfil/updateoferta/').$ofertas['cod_o'] ?>" method="post" enctype="multipart/form-data">
      <input type="text" id="titulo" name="titulo" placeholder="Título da Oferta*" value="<?= isset($ofertas) ? $ofertas['titulo'] : "" ?>" required>
      <br><br>
      <select id="cidade" name="cidade" form="anunciovaga" required>
          <option selected><?= isset($ofertas) ? $ofertas['cidade'] : "" ?></option>
          <option value="Araricá">Araricá</option>
          <option value="Campo Bom">Campo Bom</option>
          <option value="Dois Irmãos">Dois Irmãos</option>
          <option value="Nova Hartz">Nova Hartz</option>
          <option value="Novo Hamburgo">Novo Hamburgo</option>
          <option value="Sapiranga">Sapiranga</option>
        </select> 
        <br><br>
        <p>Adicione novamente o local de trabalho*</p>
        <div class="checkboxlocais">
          <p class="titulolocais">Locais de trabalho:</p>
          <div><input type="checkbox" name="locais[]"  value="Presencial"><label class="titulolocais">Presencial</label></div>
          <div><input type="checkbox" name="locais[]" value="Híbrido"><label class="titulolocais">Híbrido</label></div>
          <div><input type="checkbox" name="locais[]" value="Home Office"><label class="titulolocais">Home Office</label></div>
        </div>
      <br><br>
      <select id="tipooferta" name="tipooferta" form="anunciooferta" required>
        <option selected><?= isset($ofertas) ? $ofertas['tipooferta'] : "" ?></option>
        <option value="Freelancer">Freelancer</option>
        <option value="Autônomo">Autônomo</option>
        <option value="Procurando Emprego">Procurando Emprego</option>
        <option value="Microempreendedor">Microempreendedor</option>
      </select>
      <br><br>
      <div class="formmetade">
        <div><input type="text" id="preco" name="preco" placeholder="Valor (R$)" value="<?= isset($ofertas) ? $ofertas['preco'] : "" ?>" required></div>
        <div><input type="text" id="tipopreco" name="tipopreco" placeholder="Tipo (ex: por hora, por serviço, por semana, etc.)" value="<?= isset($ofertas) ? $ofertas['tipopreco'] : "" ?>" required></div>
      </div>
      <br><br>
      <textarea rows="5" id="descricao" name="descricao" placeholder="Descrição da oferta*" required><?= isset($ofertas) ? $ofertas['descricao'] : "" ?></textarea>
      <br><br>
      <input type="text" id="formacontato" name="formacontato" placeholder="Forma de contato*" value="<?= isset($ofertas) ? $ofertas['formacontato'] : "" ?>" required>
      <br><br>
      <p style="color:#B6B6B6;">* Itens obrigatórios</p>
      <input class="sendbutton" type="submit" id="enviar" value="SALVAR">
    </form>
    </div>

  </div>
</body>