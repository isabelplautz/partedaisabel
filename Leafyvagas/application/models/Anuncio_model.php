<?php

class Anuncio_model extends CI_Model {

    public function indexvaga(){
       // $this->db->get("users")->result_array();
       return $this->db->get("vagas")->result_array();
    }
    public function indexoferta(){
        //$this->db->get("users")->result_array();
        return $this->db->get("ofertas")->result_array();
     }
    public function cadastrovaga($vaga){
        $this->db->insert("vagas", $vaga);
    }
    public function cadastrooferta($oferta){
        $this->db->insert("ofertas", $oferta);
    }
} 