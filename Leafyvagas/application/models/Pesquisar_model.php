<?php

class Pesquisar_model extends CI_Model {

    public function pesquisarvagas($pesquisa){
        if(empty($pesquisa)){
            return array();
        }
        $this->db->get("users")->result_array();
        $pesquisa = $this->input->post("pesquisa");
        $resultado = $this->db->like("titulo", $pesquisa); $this->db->or_like("tipovaga", $pesquisa); $this->db->or_like("cidade", $pesquisa);
        return $this->db->get("vagas")->result_array();
        
    }
    public function pesquisarofertas($pesquisa){
        if(empty($pesquisa)){
            return array();
        }
        $this->db->get("users")->result_array();
        $pesquisa = $this->input->post("pesquisa");
        $resultado = $this->db->like("titulo", $pesquisa); $this->db->or_like("tipooferta", $pesquisa); $this->db->or_like("cidade", $pesquisa);
        return $this->db->get("ofertas")->result_array();
        
    }
    public function indexvaga(){
        $this->db->get("users")->result_array();
       return $this->db->get("vagas")->result_array();
    }

    public function indexoferta(){
        $this->db->get("users")->result_array();
        return $this->db->get("ofertas")->result_array();
     }
}