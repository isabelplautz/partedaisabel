<?php

class Perfil_model extends CI_Model {

    public function index(){
        return $this->db->get("users")->result_array();
    }

    public function show($cod_user){
        return $this->db->get_where("users", array("cod_user" => $cod_user))->row_array();
    }
    public function showvaga($cod_v){
        $vaga = $this->db->get_where("vagas", array("cod_v" => $cod_v))->row_array();
        return $vaga;
    }
    public function showoferta($cod_o){
        $oferta = $this->db->get_where("ofertas", array("cod_o" => $cod_o))->row_array();
        return $oferta;
    }
    public function update($cod_user, $user){
        $this->session->set_userdata("logged_user", $user);
        $this->db->where("cod_user", $cod_user);    
        return $this->db->update("users", $user);
    }
    public function updatevaga($cod_v, $vaga){
        $this->db->where("cod_v", $cod_v);     
        return $this->db->update("vagas", $vaga);
    }
    public function updateoferta($cod_o, $oferta){
        $this->db->where("cod_o", $cod_o);    
        return $this->db->update("ofertas", $oferta);
    }

    public function destroy($cod_user){
        $this->db->where("cod_user", $cod_user);
        return $this->db->delete("users");

    }
    public function destroyvaga($cod_user){
        $this->db->where("cod_user", $cod_user);
        return $this->db->delete("vagas");
    }
    public function destroyoferta($cod_user){
        $this->db->where("cod_user", $cod_user);
        return $this->db->delete("ofertas");
    }
    public function destroyvagaind($cod_v){
        $this->db->where("cod_v", $cod_v);
        return $this->db->delete("vagas");
    }
    public function destroyofertaind($cod_o){
        $this->db->where("cod_o", $cod_o);
        return $this->db->delete("ofertas");
    }


    
}