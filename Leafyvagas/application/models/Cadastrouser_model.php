<?php

class Cadastrouser_model extends CI_Model {

    public function index(){
        return $this->db->get("users")->result_array();
    }

    public function cadastroefetuado($user){
        $this->db->insert("users", $user);
    }

    public function show($cod_user)
    {
        return $this->db->get_where("users", array(
            "cod_user" => $cod_user
        ))->row_array();
    }
    
}